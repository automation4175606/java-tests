import org.testng.annotations.Test;
import pages.InvoicesHomePage;
import pages.LocalAddInvoicePage;

public class LocalInvoicePageTest extends BaseTest {

    @Test
    public void prepareInvoice() throws InterruptedException {

        InvoicesHomePage invoiceHomePage = new InvoicesHomePage(driver);
        LocalAddInvoicePage localAddinvoicePage = new LocalAddInvoicePage(driver);

        invoiceHomePage.openUrl("https://www.fakturowo.pl/")
                .addInvoiceButtonIsDisplayed()
                .clickOnAddInvoiceButton();

        localAddinvoicePage.addInvoicePageTitleIsDisplayed()
                .issuePlaceIsDisplayed()
                .fillIssuePlace()
                .invoiceNumberIsDisplayed()
                .fillInvoiceNumber("09/2023")
                .sellDateIsDisplayed()
                .fillSellDate("30-09-2023")

                .sellerNameFieldIsDisplayed()
                .fillSellerNameField()
                .sellerIdentifyNumberFieldIsDisplayed()
                .fillSellerIdentifyNumberField()
                .sellerStreetFieldIsDisplayed()
                .fillSellerStreetField()
                .sellerCityFieldIsDisplayed()
                .fillSellerCityField()
                .sellerPostCodeFieldIsDisplayed()
                .fillSellerPostCodeField()

                .buyerNameFieldIsDisplayed()
                .fillBuyerNameField()
                .buyerIdentifyNumberFieldIsDisplayed()
                .fillBuyerIdentifyNumberField()
                .buyerStreetFieldIsDisplayed()
                .fillBuyerStreetField()
                .buyerCityFieldIsDisplayed()
                .fillBuyerCityField()
                .buyerPostCodeFieldIsDisplayed()
                .fillBuyerPostCodeField()

                .firstServiceNameIsDisplayed()
                .fillFirstServiceNameField("Wrzesień")
                .firstServicePriceIsDisplayed()
                .fillFirstServicePriceField()
                .dateOfPaymentFieldIsDisplayed()
                .fillDateOfPaymentFieldField("30-09-2023")
                .sellerAccountFieldIsDisplayed()
                .fillSellerAccountField();
//                .scrollToTheTop()
//                .downloadButtonIsDisplayed()
//                .clickOnDownloadButton()
//                .waitForFile();
    }
}
