import pages.AddInvoicePage;
import pages.InvoicesHomePage;
import org.testng.annotations.Test;

public class invoicePageTest extends BaseTest {

    @Test
    public void prepareInvoice_1() throws InterruptedException {

        InvoicesHomePage invoiceHomePage = new InvoicesHomePage(driver);
        AddInvoicePage addinvoicePage = new AddInvoicePage(driver);

        invoiceHomePage.openUrl("https://www.fakturowo.pl/")
                .addInvoiceButtonIsDisplayed()
                .clickOnAddInvoiceButton();

        addinvoicePage.addInvoicePageTitleIsDisplayed()
                .issuePlaceIsDisplayed()
                .fillIssuePlace()
                .invoiceNumberIsDisplayed()
                .fillInvoiceNumber()
                .sellDateIsDisplayed()
                .fillSellDate()

                .sellerNameFieldIsDisplayed()
                .fillSellerNameField()
                .sellerIdentifyNumberFieldIsDisplayed()
                .fillSellerIdentifyNumberField()
                .sellerStreetFieldIsDisplayed()
                .fillSellerStreetField()
                .sellerCityFieldIsDisplayed()
                .fillSellerCityField()
                .sellerPostCodeFieldIsDisplayed()
                .fillSellerPostCodeField()

                .buyerNameFieldIsDisplayed()
                .fillBuyerNameField()
                .buyerIdentifyNumberFieldIsDisplayed()
                .fillBuyerIdentifyNumberField()
                .buyerStreetFieldIsDisplayed()
                .fillBuyerStreetField()
                .buyerCityFieldIsDisplayed()
                .fillBuyerCityField()
                .buyerPostCodeFieldIsDisplayed()
                .fillBuyerPostCodeField()

                .firstServiceNameIsDisplayed()
                .fillFirstServiceNameField()
                .firstServicePriceIsDisplayed()
                .fillFirstServicePriceField()
                .dateOfPaymentFieldIsDisplayed()
                .fillDateOfPaymentFieldField()
                .sellerAccountFieldIsDisplayed()
                .fillSellerAccountField()
                .downloadButtonIsDisplayed();
//                .clickOnDownloadButton();
    }
}
