import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    WebDriver driver;

    @BeforeMethod
    protected void beforeMethod() {

        String chromeDriverPath = System.getProperty("user.dir");
        String os = System.getProperty("os.name").toLowerCase();
        System.out.println(chromeDriverPath);
        System.out.println(os);
        if(os.contains("linux")) {
            System.setProperty("webdriver.chrome.driver", chromeDriverPath + "/chromedriverlinux");
        }
        else if(os.contains("mac")) {
            System.setProperty("webdriver.chrome.driver", chromeDriverPath + "/chromedrivermac");
        }
        else {
            System.setProperty("webdriver.chrome.driver", chromeDriverPath + "\\chromedriver.exe");
        }
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
    }

    @AfterMethod
    protected void afterMethod() {
        driver.manage().deleteAllCookies();
        driver.close();
        driver.quit();
    }
}
