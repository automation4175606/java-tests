package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void open(String url) {
        driver.get(url);
    }

    public void elementIsDisplayed(By element) {
        driver.findElement(element).isDisplayed();
    }

    public void clickOnElement(By element) {
        driver.findElement(element).click();
    }

    public void sendKeysToElement(By element, String keys) {
        driver.findElement(element).sendKeys(keys);
    }

    public void pressKeysOnElement(By element, Keys ...keys) {
        driver.findElement(element).sendKeys(keys);
    }

    public void clearElement(By element) {
        driver.findElement(element).clear();
    }

    public void waitForElement(By element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(driver -> driver.findElement(element));
    }
}
