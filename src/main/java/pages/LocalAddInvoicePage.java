package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class LocalAddInvoicePage extends BasePage {

    By addInvoicePageTitle = By.xpath("//h4[@class='black']/a[@title='Faktura VAT']");
    By placeField = By.cssSelector("[name='miejsce']");
    By invoiceNumberField = By.cssSelector("[name='numer']");
    By sellDateField = By.cssSelector("[name='data_s']");

    By sellerNameField = By.cssSelector("[name='sprzedawca[nazwa]']");
    By sellerIdentifyNumberField = By.cssSelector("[name='sprzedawca[nip]']");
    By sellerStreetField = By.cssSelector("[name='sprzedawca[ulica]']");
    By sellerCityField = By.cssSelector("[name='sprzedawca[miasto]']");
    By sellerPostCodeField = By.cssSelector("[name='sprzedawca[kod]']");

    By buyerNameField = By.cssSelector("[name='nabywca[nazwa]']");
    By buyerIdentifyNumberField = By.cssSelector("[name='nabywca[nip]']");
    By buyerStreetField = By.cssSelector("[name='nabywca[ulica]']");
    By buyerCityField = By.cssSelector("[name='nabywca[miasto]']");
    By buyerPostCodeField = By.cssSelector("[name='nabywca[kod]']");

    By firstServiceNameField = By.cssSelector("[name='nazwa[0]']");
    By firstServicePriceField = By.cssSelector("[name='cena_netto[0]']");
    By dateOfPaymentField = By.cssSelector("[name='termin']");
    By sellerAccountField = By.cssSelector("[name='sprzedawca[konto]']");

    By downloadButton = By.cssSelector("[id='pobierz']");

    public LocalAddInvoicePage(WebDriver driver) {
        super(driver);
    }

    public LocalAddInvoicePage openUrl(String url) {
        open(url);
        return this;
    }

    public LocalAddInvoicePage addInvoicePageTitleIsDisplayed() {
        elementIsDisplayed(addInvoicePageTitle);
        return this;
    }

    public LocalAddInvoicePage issuePlaceIsDisplayed() {
        elementIsDisplayed(placeField);
        return this;
    }

    public LocalAddInvoicePage fillIssuePlace() {
        sendKeysToElement(placeField, "Kraków");
        return this;
    }

    public LocalAddInvoicePage invoiceNumberIsDisplayed() {
        elementIsDisplayed(invoiceNumberField);
        return this;
    }

    public LocalAddInvoicePage fillInvoiceNumber(String number) {
        clearElement(invoiceNumberField);
        sendKeysToElement(invoiceNumberField, number);
        return this;
    }

    public LocalAddInvoicePage sellDateIsDisplayed() {
        elementIsDisplayed(sellDateField);
        return this;
    }

    public LocalAddInvoicePage fillSellDate(String date) {
        clearElement(sellDateField);
        sendKeysToElement(sellDateField, date);
        return this;
    }

    public LocalAddInvoicePage sellerNameFieldIsDisplayed() {
        elementIsDisplayed(sellerNameField);
        return this;
    }

    public LocalAddInvoicePage fillSellerNameField() {
        sendKeysToElement(sellerNameField, "Usługi Informatyczne Dariusz Steblik");
        return this;
    }

    public LocalAddInvoicePage sellerIdentifyNumberFieldIsDisplayed() {
        elementIsDisplayed(sellerIdentifyNumberField);
        return this;
    }

    public LocalAddInvoicePage fillSellerIdentifyNumberField() {
        sendKeysToElement(sellerIdentifyNumberField, "5532557958");
        return this;
    }

    public LocalAddInvoicePage sellerStreetFieldIsDisplayed() {
        elementIsDisplayed(sellerStreetField);
        return this;
    }

    public LocalAddInvoicePage fillSellerStreetField() {
        sendKeysToElement(sellerStreetField, "Długa 34");
        return this;
    }

    public LocalAddInvoicePage sellerCityFieldIsDisplayed() {
        elementIsDisplayed(sellerCityField);
        return this;
    }

    public LocalAddInvoicePage fillSellerCityField() {
        sendKeysToElement(sellerCityField, "Żywiec");
        return this;
    }

    public LocalAddInvoicePage sellerPostCodeFieldIsDisplayed() {
        elementIsDisplayed(sellerPostCodeField);
        return this;
    }

    public LocalAddInvoicePage fillSellerPostCodeField() {
        sendKeysToElement(sellerPostCodeField, "34-300");
        return this;
    }

    public LocalAddInvoicePage buyerNameFieldIsDisplayed() {
        elementIsDisplayed(buyerNameField);
        return this;
    }

    public LocalAddInvoicePage fillBuyerNameField() {
        sendKeysToElement(buyerNameField, "GRAND PARADE SP Z O.O.");
        return this;
    }

    public LocalAddInvoicePage buyerIdentifyNumberFieldIsDisplayed() {
        elementIsDisplayed(buyerIdentifyNumberField);
        return this;
    }

    public LocalAddInvoicePage fillBuyerIdentifyNumberField() {
        sendKeysToElement(buyerIdentifyNumberField, "6762437299");
        return this;
    }

    public LocalAddInvoicePage buyerStreetFieldIsDisplayed() {
        elementIsDisplayed(buyerStreetField);
        return this;
    }

    public LocalAddInvoicePage fillBuyerStreetField() {
        sendKeysToElement(buyerStreetField, "KOTLARSKA 11");
        return this;
    }

    public LocalAddInvoicePage buyerCityFieldIsDisplayed() {
        elementIsDisplayed(buyerCityField);
        return this;
    }

    public LocalAddInvoicePage fillBuyerCityField() {
        sendKeysToElement(buyerCityField, "KRAKÓW");
        return this;
    }

    public LocalAddInvoicePage buyerPostCodeFieldIsDisplayed() {
        elementIsDisplayed(buyerPostCodeField);
        return this;
    }

    public LocalAddInvoicePage fillBuyerPostCodeField() {
        sendKeysToElement(buyerPostCodeField, "31-539");
        return this;
    }

    public LocalAddInvoicePage firstServiceNameIsDisplayed() {
        elementIsDisplayed(firstServiceNameField);
        return this;
    }

    public LocalAddInvoicePage fillFirstServiceNameField(String month) {
        sendKeysToElement(firstServiceNameField, "Usługa IT - " + month + " 2023");
        return this;
    }

    public LocalAddInvoicePage firstServicePriceIsDisplayed() {
        elementIsDisplayed(firstServicePriceField);
        return this;
    }

    public LocalAddInvoicePage fillFirstServicePriceField() {
        clickOnElement(firstServicePriceField);
        pressKeysOnElement(firstServicePriceField, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
        sendKeysToElement(firstServicePriceField, "17350.00");
        return this;
    }

    public LocalAddInvoicePage dateOfPaymentFieldIsDisplayed() {
        elementIsDisplayed(dateOfPaymentField);
        return this;
    }

    public LocalAddInvoicePage fillDateOfPaymentFieldField(String date) {
        sendKeysToElement(dateOfPaymentField, date);
        return this;
    }

    public LocalAddInvoicePage sellerAccountFieldIsDisplayed() {
        elementIsDisplayed(sellerAccountField);
        return this;
    }

    public LocalAddInvoicePage fillSellerAccountField() {
        sendKeysToElement(sellerAccountField, "10116022020000000458409141");
        return this;
    }

    public LocalAddInvoicePage downloadButtonIsDisplayed() {
        elementIsDisplayed(downloadButton);
        return this;
    }

    public LocalAddInvoicePage scrollToTheTop()  {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,-250)");
        return this;
    }

    public LocalAddInvoicePage clickOnDownloadButton() {
        clickOnElement(downloadButton);
        return this;
    }

    public LocalAddInvoicePage waitForFile() throws InterruptedException {
        Thread.sleep(3000);
        return this;
    }
}
