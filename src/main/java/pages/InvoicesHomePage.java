package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InvoicesHomePage extends BasePage {

    By addInvoice = By.xpath("//*[contains(text(), 'wystaw fakturę')]");

    public InvoicesHomePage(WebDriver driver) {
        super(driver);
    }

    public InvoicesHomePage openUrl(String url) {
        open(url);
        return this;
    }

    public InvoicesHomePage addInvoiceButtonIsDisplayed() {
        elementIsDisplayed(addInvoice);
        return this;
    }

    public InvoicesHomePage clickOnAddInvoiceButton() {
        clickOnElement(addInvoice);
        return this;
    }
}
