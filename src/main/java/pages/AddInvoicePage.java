package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class AddInvoicePage extends BasePage {

    By addInvoicePageTitle = By.xpath("//h4[@class='black']/a[@title='Faktura VAT']");
    By placeField = By.cssSelector("[name='miejsce']");
    By invoiceNumberField = By.cssSelector("[name='numer']");
    By sellDateField = By.cssSelector("[name='data_s']");

    By sellerNameField = By.cssSelector("[name='sprzedawca[nazwa]']");
    By sellerIdentifyNumberField = By.cssSelector("[name='sprzedawca[nip]']");
    By sellerStreetField = By.cssSelector("[name='sprzedawca[ulica]']");
    By sellerCityField = By.cssSelector("[name='sprzedawca[miasto]']");
    By sellerPostCodeField = By.cssSelector("[name='sprzedawca[kod]']");

    By buyerNameField = By.cssSelector("[name='nabywca[nazwa]']");
    By buyerIdentifyNumberField = By.cssSelector("[name='nabywca[nip]']");
    By buyerStreetField = By.cssSelector("[name='nabywca[ulica]']");
    By buyerCityField = By.cssSelector("[name='nabywca[miasto]']");
    By buyerPostCodeField = By.cssSelector("[name='nabywca[kod]']");

    By firstServiceNameField = By.cssSelector("[name='nazwa[0]']");
    By firstServicePriceField = By.cssSelector("[name='cena_netto[0]']");
    By dateOfPaymentField = By.cssSelector("[name='termin']");
    By sellerAccountField = By.cssSelector("[name='sprzedawca[konto]']");

    By downloadButton = By.cssSelector("[id='pobierz']");

    public AddInvoicePage(WebDriver driver) {
        super(driver);
    }

    public AddInvoicePage openUrl(String url) {
        open(url);
        return this;
    }

    public AddInvoicePage addInvoicePageTitleIsDisplayed() {
        elementIsDisplayed(addInvoicePageTitle);
        return this;
    }

    public AddInvoicePage issuePlaceIsDisplayed() {
        elementIsDisplayed(placeField);
        return this;
    }

    public AddInvoicePage fillIssuePlace() {
        sendKeysToElement(placeField, "Cracow");
        return this;
    }

    public AddInvoicePage invoiceNumberIsDisplayed() {
        elementIsDisplayed(invoiceNumberField);
        return this;
    }

    public AddInvoicePage fillInvoiceNumber() {
        clearElement(invoiceNumberField);
        sendKeysToElement(invoiceNumberField, "02/2023");
        return this;
    }

    public AddInvoicePage sellDateIsDisplayed() {
        elementIsDisplayed(sellDateField);
        return this;
    }

    public AddInvoicePage fillSellDate() {
        clearElement(sellDateField);
        sendKeysToElement(sellDateField, "28-02-2023");
        return this;
    }

    public AddInvoicePage sellerNameFieldIsDisplayed() {
        elementIsDisplayed(sellerNameField);
        return this;
    }

    public AddInvoicePage fillSellerNameField() {
        sendKeysToElement(sellerNameField, "Usługi Informatyczne John Wick");
        return this;
    }

    public AddInvoicePage sellerIdentifyNumberFieldIsDisplayed() {
        elementIsDisplayed(sellerIdentifyNumberField);
        return this;
    }

    public AddInvoicePage fillSellerIdentifyNumberField() {
        sendKeysToElement(sellerIdentifyNumberField, "5532557111");
        return this;
    }

    public AddInvoicePage sellerStreetFieldIsDisplayed() {
        elementIsDisplayed(sellerStreetField);
        return this;
    }

    public AddInvoicePage fillSellerStreetField() {
        sendKeysToElement(sellerStreetField, "Prosta 10");
        return this;
    }

    public AddInvoicePage sellerCityFieldIsDisplayed() {
        elementIsDisplayed(sellerCityField);
        return this;
    }

    public AddInvoicePage fillSellerCityField() {
        sendKeysToElement(sellerCityField, "Katowice");
        return this;
    }

    public AddInvoicePage sellerPostCodeFieldIsDisplayed() {
        elementIsDisplayed(sellerPostCodeField);
        return this;
    }

    public AddInvoicePage fillSellerPostCodeField() {
        sendKeysToElement(sellerPostCodeField, "34-100");
        return this;
    }

    public AddInvoicePage buyerNameFieldIsDisplayed() {
        elementIsDisplayed(buyerNameField);
        return this;
    }

    public AddInvoicePage fillBuyerNameField() {
        sendKeysToElement(buyerNameField, "Firma Sp. z o.o.");
        return this;
    }

    public AddInvoicePage buyerIdentifyNumberFieldIsDisplayed() {
        elementIsDisplayed(buyerIdentifyNumberField);
        return this;
    }

    public AddInvoicePage fillBuyerIdentifyNumberField() {
        sendKeysToElement(buyerIdentifyNumberField, "6762437211");
        return this;
    }

    public AddInvoicePage buyerStreetFieldIsDisplayed() {
        elementIsDisplayed(buyerStreetField);
        return this;
    }

    public AddInvoicePage fillBuyerStreetField() {
        sendKeysToElement(buyerStreetField, "Adama Mickiewicza 12");
        return this;
    }

    public AddInvoicePage buyerCityFieldIsDisplayed() {
        elementIsDisplayed(buyerCityField);
        return this;
    }

    public AddInvoicePage fillBuyerCityField() {
        sendKeysToElement(buyerCityField, "Cracow");
        return this;
    }

    public AddInvoicePage buyerPostCodeFieldIsDisplayed() {
        elementIsDisplayed(buyerPostCodeField);
        return this;
    }

    public AddInvoicePage fillBuyerPostCodeField() {
        sendKeysToElement(buyerPostCodeField, "31-500");
        return this;
    }

    public AddInvoicePage firstServiceNameIsDisplayed() {
        elementIsDisplayed(firstServiceNameField);
        return this;
    }

    public AddInvoicePage fillFirstServiceNameField() {
        sendKeysToElement(firstServiceNameField, "Usługa IT - Luty 2023");
        return this;
    }

    public AddInvoicePage firstServicePriceIsDisplayed() {
        elementIsDisplayed(firstServicePriceField);
        return this;
    }

    public AddInvoicePage fillFirstServicePriceField() {
        clickOnElement(firstServicePriceField);
        pressKeysOnElement(firstServicePriceField, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
        sendKeysToElement(firstServicePriceField, "15000.00");
        return this;
    }

    public AddInvoicePage dateOfPaymentFieldIsDisplayed() {
        elementIsDisplayed(dateOfPaymentField);
        return this;
    }

    public AddInvoicePage fillDateOfPaymentFieldField() {
        sendKeysToElement(dateOfPaymentField, "28-02-2023");
        return this;
    }

    public AddInvoicePage sellerAccountFieldIsDisplayed() {
        elementIsDisplayed(sellerAccountField);
        return this;
    }

    public AddInvoicePage fillSellerAccountField() {
        sendKeysToElement(sellerAccountField, "30870210241200724376380021");
        return this;
    }

    public AddInvoicePage downloadButtonIsDisplayed() {
        elementIsDisplayed(downloadButton);
        return this;
    }

    public AddInvoicePage clickOnDownloadButton() {
        clickOnElement(downloadButton);
        return this;
    }
}
